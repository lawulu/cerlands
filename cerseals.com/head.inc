<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.selectbox.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="assets/cerseals.css">
<link rel="stylesheet" id="color-scheme" href="css/colors/green.css">


<!-- Modernizr -->
<script src="js/modernizr.js"></script>

<!--- jQuery -->
<script src="js/jquery.min.js"></script>

<!-- Queryloader -->
<script src="js/queryloader2.min.js"></script>

